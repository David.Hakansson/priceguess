import react from 'react'
import {Button,Col} from 'react-bootstrap'

function PriceGuess(){

    //hanterar klickeliklick i supercooladavidprisgissarspelet!
    
    function clickHandler(){
        alert(" knapp tryckt")
    }

    return(
        <div class="row">
          <div className="LeftCar">
            <img src="https://picsum.photos/500/300"/>
            <p>Volvo V70 740hk Bötat</p>
            <Button onClick={clickHandler}>Denna</Button>
          </div>
          <div className="RightCar">
            <img src="https://picsum.photos/500/301"/>
            <p>Volvo S60 2.4 Momentum 140hk NYBESIKTIGAD</p>
            <Button>Denna</Button>
          </div>
        </div>
    )
}

export default PriceGuess
