import React, { Component } from 'react';
import {Button,Col} from 'react-bootstrap'

class PriceGuessComponent extends Component {

    constructor(props){
        super(props)

        this.state = {
            counter : 1,
            leftImgValues: {
                imgscr: "https://picsum.photos/500/300",
                title: "Volvo V70 740hk Bötat",
                price: 5000
            },
            rightImgValues: {
                imgscr: "https://picsum.photos/500/301",
                title: "Den här skitfula saaben",
                price: 1000
            }
        }
    }

    leftImgClickHandler(){
        if (this.state.leftImgValues.price > this.state.rightImgValues.price){
            
            //TODO ta bort alerts om det ska vara möjligt att spela på mobil
            this.setState({
                counter : this.state.counter + 1,
                rightImgValues : {
                    imgscr: "https://picsum.photos/500/30"+this.state.counter,
                    title: "Den här ännu skitfulare saaben",
                    price: 2
                }
            })
            alert("Rätt, men av dessa två då? Du har gissat rätt: " + this.state.counter + " gånger i rad");
            
        }
        else{
            alert("FEL!, du får börja om från 0")
            this.setState({
                counter : 0,
            })
        }
    }

    rightImgClickHandler(){
        if (this.state.leftImgValues.price < this.state.rightImgValues.price){
            //TODO ta bort alerts om det ska vara möjligt att spela på mobil
            this.setState({leftImgValues : {
                imgscr: "https://picsum.photos/500/302+this.state.counter",
                title: "Den här ännu skitfulare saaben",
                price: 2
                }
            })
            alert("Rätt, men av dessa två då? Du har gissat rätt: " + this.state.counter + " gånger i rad");

        }
        else{
            alert("FEL!, du får börja om från 0")
            this.setState({
                counter : 0,
            })
        }
    }

    render() {
        return (
            <div>
                <div class="row">
                    <div className="LeftCar">
                        <img src={this.state.leftImgValues.imgscr}/>
                        <p>{this.state.leftImgValues.title}</p>
                        <Button onClick={() => this.leftImgClickHandler()}>Denna</Button>
                    </div>
                    <div className="RightCar">
                        <img src={this.state.rightImgValues.imgscr}/>
                        <p>{this.state.rightImgValues.title}</p>
                        <Button onClick={() => this.rightImgClickHandler()}> Denna</Button>
                    </div>
                </div>               
            </div>
        );
    }
}

export default PriceGuessComponent;